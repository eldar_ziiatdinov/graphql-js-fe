import React from 'react';
import { ErrorBoundary } from './components/error-boundary';
import { Router } from './router';
import { ApolloProvider } from 'react-apollo';
import { client } from './graphql/apollo-client';

export const App = () => {
  return (
    <ErrorBoundary>
      <ApolloProvider client={client}>
        <Router />
      </ApolloProvider>
    </ErrorBoundary>
  );
};
