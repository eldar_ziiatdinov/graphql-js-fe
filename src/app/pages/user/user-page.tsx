import React from 'react';
import { CreateUserForm } from './create-user-form';
import { RemoveUserForm } from './remove-user-form';

export const UserPage = () => {
  return (
    <>
      <section>
        <CreateUserForm />
      </section>
      <section>
        <RemoveUserForm/>
      </section>
    </>
  );
};
