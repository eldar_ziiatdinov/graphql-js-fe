import React, { useState } from 'react';
import { Select } from '../../components/select';
import { Button } from '../../components/button';
import { getDictionary } from '../../dictionary';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { usersQuery } from '../../graphql/queries';
import { User, Users } from '../../graphql/types';
import { map } from 'lodash';
import { removeUserMutation } from '../../graphql/mutations';
import { Label } from '../../components/label';

const FIELD_NAMES = {
  USERS: 'user.users',
};

export const RemoveUserForm = () => {
  const [selectedUser, setSelectedUser] = useState();
  const { data, loading, refetch } = useQuery<Users>(usersQuery);
  const [removeUser] = useMutation(removeUserMutation);

  const fetchedUsers = loading || !data ? [] : data.users;
  const items = map(fetchedUsers, user => ({
    id: user.id,
    label: `${user.surname} ${user.name}`,
    value: user,
  }));

  const handleSelectUser = (user: User) => setSelectedUser(user);

  const handleClickRemoveUser = () => {
    if (selectedUser) {
      removeUser({
        variables: {
          id: selectedUser.id,
        },
      })
        .then(value => {
          if (value.data) {
            console.log('user has been removed:');
            console.log(value.data);
          } else {
            console.error('user has not been removed');
            console.error(value.errors);
          }
          setSelectedUser(null);
          refetch();
        })
        .catch(reason => {
          console.error('user has not been removed');
          console.error(reason);
        });
    } else {
      console.warn('You did not select user');
    }
  };

  return (
    <form>
      <Label value={getDictionary('user.select.user')}>
        <Select name={FIELD_NAMES.USERS} items={items} onSelect={handleSelectUser} />
      </Label>
      <Button value={getDictionary('user.button.remove')} onClick={handleClickRemoveUser} />
    </form>
  );
};
