import React, { useState } from 'react';
import { TextInput } from '../../components/input/text';
import { Label } from '../../components/label';
import { getDictionary } from '../../dictionary';
import { Button } from '../../components/button';
import { useMutation } from '@apollo/react-hooks';
import { createUserMutation } from '../../graphql/mutations';
import { User } from '../../graphql/types';

const FIELD_NAMES = {
  NAME: 'user.name',
  SURNAME: 'user.surname',
  MIDDLENAME: 'user.middlename',
  EMAIL: 'user.email',
  PHONE: 'user.phone',
};

export const CreateUserForm = () => {
  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [middlename, setMiddlename] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [createUser] = useMutation<User>(createUserMutation);

  const handleChangeName = (value: string) => setName(value);
  const handleChangeSurname = (value: string) => setSurname(value);
  const handleChangeMiddlename = (value: string) => setMiddlename(value);
  const handleChangePhone = (value: string) => setPhone(value);
  const handleChangeEmail = (value: string) => setEmail(value);

  const handleClickCreateUser = () => {
    createUser({
      variables: {
        name,
        surname,
        middlename,
        phone,
        email,
      },
    })
      .then(value => {
        if (value.data) {
          console.log('user has been created:');
          console.log(value.data);
        } else {
          console.error('user has not been created');
          console.error(value.errors);
        }
      })
      .catch(reason => {
        console.error('user has not been created');
        console.error(reason);
      });
  };

  return (
    <form>
      <Label value={getDictionary(FIELD_NAMES.NAME)}>
        <TextInput name={FIELD_NAMES.NAME} onChange={handleChangeName} />
      </Label>
      <Label value={getDictionary(FIELD_NAMES.SURNAME)}>
        <TextInput name={FIELD_NAMES.SURNAME} onChange={handleChangeSurname} />
      </Label>
      <Label value={getDictionary(FIELD_NAMES.MIDDLENAME)}>
        <TextInput name={FIELD_NAMES.MIDDLENAME} onChange={handleChangeMiddlename} />
      </Label>
      <Label value={getDictionary(FIELD_NAMES.PHONE)}>
        <TextInput name={FIELD_NAMES.PHONE} onChange={handleChangePhone} />
      </Label>
      <Label value={getDictionary(FIELD_NAMES.EMAIL)}>
        <TextInput name={FIELD_NAMES.EMAIL} onChange={handleChangeEmail} />
      </Label>
      <Button value={getDictionary('user.button.create')} onClick={handleClickCreateUser} />
    </form>
  );
};
