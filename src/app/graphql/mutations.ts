import { gql } from 'apollo-boost';

export const createUserMutation = gql`
  mutation CreateUser($name: String!, $surname: String!, $middlename: String!, $phone: String!, $email: String!) {
    user(name: $name, surname: $surname, middlename: $middlename, phone: $phone, email: $email) {
      id
      name
      surname
      middlename
      phone
      email
    }
  }
`;

export const removeUserMutation = gql`
  mutation RemoveUser($id: ID!) {
    removeUser(id: $id) {
      name
      surname
      middlename
      phone
      email
    }
  }
`;
