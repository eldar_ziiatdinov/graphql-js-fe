import {gql} from 'apollo-boost';

export const usersQuery = gql`
    query Users {
        users {
            id
            name
            surname
            middlename
            phone
            email
        }
    }
`;

export const servicesQuery = gql`
    query Services {
        services {
            id
            price
            name
            description
            photos
        }
    }
`;
