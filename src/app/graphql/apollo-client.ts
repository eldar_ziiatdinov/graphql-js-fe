import {ApolloClient, InMemoryCache, HttpLink} from 'apollo-boost';

export const cache = new InMemoryCache();
export const client = new ApolloClient({
  name: 'react-web-client',
  cache,
  link: new HttpLink({
    uri: '/graphql',
  })
});
