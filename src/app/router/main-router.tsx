import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { UserPage } from '../pages/user/user-page';

export const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/user" component={UserPage} />
      <Redirect from="*" to="/user" />
    </Switch>
  </BrowserRouter>
);
