import {get} from 'lodash';
import dictionary from './dictionary.json';

export const getDictionary = (path: string) => get(dictionary, path);
