import React, { MouseEvent } from 'react';

interface ButtonProps {
  value: string;
  onClick: () => void;
  className?: string;
}

export const Button = ({ value, onClick, className }: ButtonProps) => {
  const handleClick = (e: MouseEvent) => {
    e.preventDefault();
    onClick();
  };

  return (
    <button onClick={handleClick} className={className}>
      {value}
    </button>
  );
};
