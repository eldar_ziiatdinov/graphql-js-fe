import React from 'react';
import {Label} from './label';
import {shallow} from 'enzyme';

describe('Label', () => {
  it('shallow render', () => {
    const forId = 'test';
    const value = 'Test';
    const wrapper = shallow(<Label forId={forId} value={value} />);
    expect(wrapper).toMatchSnapshot();
  });
});
