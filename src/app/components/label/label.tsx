import React from 'react';

interface LabelProps {
  forId?: string;
  value: string;
  onClick?: () => void;
  children?: React.ReactChild;
}

export const Label = ({ forId, value, children, onClick }: LabelProps) => (
  <label htmlFor={forId}>
    <span onClick={onClick}>{value}</span>
    {children}
  </label>
);
