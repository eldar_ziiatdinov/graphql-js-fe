import React, { ChangeEvent, useState } from 'react';

export interface TextInputProps {
  name: string;
  onChange: (value: string) => any;
  placeholder?: string;
  mask?: string;
  value?: string;
}

export const TextInput = ({ onChange, placeholder, value: initValue, name }: TextInputProps) => {
  const [value, setValue] = useState(initValue || '');

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setValue(value);
    onChange(value);
  };

  return <input name={name} type="text" onChange={handleChange} placeholder={placeholder} value={value} />;
};
