import React, { ChangeEvent, useState } from 'react';

export interface NumberInputProps {
  name: string;
  onChange: (value: number) => any;
  placeholder?: string;
  mask?: string;
  value?: number;
}

export const NumberInput = ({ name, onChange, value: initValue, placeholder }: NumberInputProps) => {
  const [value, setValue] = useState(initValue || 0);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setValue(Number(value));
    onChange(Number(value));
  };

  return <input type="number" placeholder={placeholder} name={name} value={value} onChange={handleChange} />;
};
